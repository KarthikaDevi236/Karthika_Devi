

const express = require('express');
const bodyParser = require('body-parser');
const url = require('url');
const app = express();
app.use(bodyParser.json())

// API sum up two numbers
app.get('/process_get', function (req, res) {
   res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
   response=parseInt(req.query.num1)+parseInt(req.query.num2);

   res.json(response);
});
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});
