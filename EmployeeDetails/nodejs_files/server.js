var express = require('express');
// var cors = require('cors');
var mongo = require('mongodb');

var app = express();

mongo.connect('mongodb://localhost:27017/example', function(err, db) {
	db.createCollection('employee');
	var collectionName = db.collection('employee');

	var saveObject = {};

	app.get('/save/:q1/:q2/:q3/:q4/:q5/:q6', function(req, res) {
		var query1 = req.params.q1;
    var query2 = req.params.q2;
    var query3 = req.params.q3;
    var query4 = req.params.q4;
    var query5 = req.params.q5;
    var query6 = req.params.q6;

		saveObject = {
			"name": query1,
			"position": query2,
      "office":query3,
      "age":query4,
      "startdate":query5,
      "salary":query6
		}
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		res.send(saveObject);
		// res.header("Access-Control-Allow-Origin", "*");
		// res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		collectionName.save(saveObject, function(err) {
			if (err) throw err;
		})
	})

	app.get('/find', function(req, res) {
		 // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080/try');
		 res.header("Access-Control-Allow-Origin", "*");
		 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		db.collection('employee', function (err, collection) {
				 collection.find({}, { _id: 0, name: 1, position: 1,office:1,age:1,startdate:1,salary:1 }).toArray(function(err, items) {
						if(err) throw err;
						res.send(items);
				});
		});
	})

	app.get('/find/:range', function(req, res) {
		 // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080/try');
		 res.header("Access-Control-Allow-Origin", "*");
		 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		db.collection('employee', function (err, collection) {
         collection.find({}, { _id: 0, name: 1, position: 1,office:1,age:1,startdate:1,salary:1 }).limit(parseInt(req.params.range)).toArray(function(err, items) {
            if(err) throw err;
						res.send(items);
        });
    });
	})
	app.get('/sort', function(req, res) {
		 // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080/try');
		 res.header("Access-Control-Allow-Origin", "*");
		 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		db.collection('employee', function (err, collection) {
				 collection.find({}, { _id: 0, name: 1, position: 1,office:1,age:1,startdate:1,salary:1 }).sort( { $natural: 1 } ).toArray(function(err, items) {
						if(err) throw err;
						res.send(items);
				});
		});
	})

	app.get('/sort/:sortcol', function(req, res) {
		 // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080/try');
		 res.header("Access-Control-Allow-Origin", "*");
		 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		db.collection('employee', function (err, collection) {
			var x=req.params.sortcol;
			var y={[x]:1};
				 collection.find({}, { _id: 0, name: 1, position: 1,office:1,age:1,startdate:1,salary:1 }).sort( y ).toArray(function(err, items) {
						if(err) throw err;
						res.send(items);
				});
		});
	})

})

app.listen(3000, function() {
	console.log('Node.js listening on port 3000');
});
